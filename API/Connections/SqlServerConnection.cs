using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Connections
{

    public class SqlServerConnection
    {
        /* Adicionar o pacote "dotnet add package System.Data.SqlClient" */

        //Definir a connection string SqlServer
        private string _connectionString = "Server=localhost\\SQLEXPRESS; Integrated Security=True";

        //Definir o nome do banco de dados
        private string _catalog = "APIPagamentosDIO";

        //Definir se as tabelas devem ser criadas durante a execução
        private bool _criarTabelas = true;

        //Configurando a conexão
        private SqlConnection _connection = new SqlConnection();
        private SqlCommand _cmd = new SqlCommand();
        private Tabelas tabelas = new Tabelas();

        //Método Conectar
        public bool Conectar()
        {
            if (_connection.State == System.Data.ConnectionState.Closed)
            {
                try
                {
                    _connection.ConnectionString = $"{_connectionString}; Initial Catalog={_catalog}";
                    _connection.Open();
                    _cmd.Connection = _connection;

                    if (VerificaTabelas())
                    {
                        return true;
                    }
                    Console.WriteLine($"Não foram encontradas todas as tabelas no Banco de Dados");

                    return false;
                }
                catch (SqlException e)
                {
                    Console.WriteLine($"Não foi possível conectar no DB {_catalog}");
                    Console.WriteLine(e);
                }

            }
            return false;
        }

        //Metodo Desconectar
        public bool Desconetar()
        {
            if (_connection.State == System.Data.ConnectionState.Open)
            {
                _connection.Close();
                return true;
            }
            return false;
        }

        //Verifica se as tabelas estão criadas no banco de dados
        private bool VerificaTabelas()
        {
            return (
                VerificaTabela("vendedores") &&
                VerificaTabela("produtos") &&
                VerificaTabela("vendas")
            );
        }

        private bool VerificaTabela(string tabela)
        {
            try
            {
                _cmd.CommandText = $"SELECT COUNT(*) FROM {tabela}";
                _cmd.ExecuteNonQuery();

                Console.WriteLine($"Tabela {tabela} OK");

                return true;
            }
            catch (SqlException)
            {
                //Cria a tabela
                if (_criarTabelas)
                {
                    try
                    {
                        Console.WriteLine($"Criando tabela {tabela}");
                        _cmd.CommandText = tabelas.tabelas[tabela];
                        _cmd.ExecuteNonQuery();

                        Console.WriteLine($"Tabela {tabela} criada");

                        return true;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Não foi possivel criar tabela {tabela}");
                        Console.WriteLine(e.Message);

                        return false;
                    }
                }
                Console.WriteLine($"Não encontrado tabela {tabela}");
                return false;
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------//
        //Cadastrar Vendedor
        public bool CadastrarVendedor(Vendedor vendedor)
        {
            try
            {
                _cmd.CommandText = "INSERT INTO vendedores VALUES (@Nome, @SobreNome)";

                _cmd.Parameters.AddWithValue("@Nome", vendedor.Nome);
                _cmd.Parameters.AddWithValue("@SobreNome", vendedor.Sobrenome);

                _cmd.ExecuteNonQuery();

                Console.WriteLine("Cadastrar vendedor - OK");
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Erro ao cadastrar vendedor");

                return false;
            }
            finally
            {
                Desconetar();
            }
        }

        //Listar todos Vendedores
        public List<Vendedor> ObterVendedores()
        {
            try
            {
                _cmd.CommandText = "SELECT * FROM vendedores";

                SqlDataReader reader = _cmd.ExecuteReader();

                List<Vendedor> vendedores = new List<Vendedor>();

                while (reader.Read())
                {
                    Vendedor vendedor = new Vendedor();
                    vendedor.id = (int)reader[0];
                    vendedor.Nome = (string)reader[1];
                    vendedor.Sobrenome = (string)reader[2];

                    vendedores.Add(vendedor);
                }
                return vendedores;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Erro carregar vendedores");
                return null;
            }
            finally
            {
                //Desconetar();
            }
        }

        //Listar Vendedor pela ID
        public Vendedor ObterVendedoresPelaId(int id)
        {
            try
            {
                _cmd.CommandText = $"SELECT * FROM vendedores WHERE id ={id}";

                SqlDataReader reader = _cmd.ExecuteReader();
                reader.Read();

                Vendedor vendedor = new Vendedor();
                vendedor.id = (int)reader[0];
                vendedor.Nome = (string)reader[1];
                vendedor.Sobrenome = (string)reader[2];

                reader.Close();
                return vendedor;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Erro carregar vendedor");
                return null;
            }
            finally
            {
                //Desconetar();
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------//
        //Cadastrar Produto
        public bool CadastrarProduto(Produto produto)
        {
            try
            {
                _cmd.CommandText = "INSERT INTO produtos VALUES (@Descricao, @Valor)";

                _cmd.Parameters.AddWithValue("@Descricao", produto.Descricao);
                _cmd.Parameters.AddWithValue("@Valor", produto.Valor);

                _cmd.ExecuteNonQuery();

                Console.WriteLine("Cadastrar produto - OK");
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Erro ao cadastrar produto");

                return false;
            }
            finally
            {
                Desconetar();
            }
        }

        //Listar todos Produtos
        public List<Produto> ObterProdutos()
        {
            try
            {
                _cmd.CommandText = "SELECT * FROM produtos";

                SqlDataReader reader = _cmd.ExecuteReader();

                List<Produto> produtos = new List<Produto>();

                while (reader.Read())
                {
                    Produto produto = new Produto();
                    produto.id = (int)reader[0];
                    produto.Descricao = (string)reader[1];
                    produto.Valor = (decimal)reader[2];

                    produtos.Add(produto);
                }
                return produtos;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Erro carregar produtos");
                return null;
            }
            finally
            {
                Desconetar();
            }
        }

        //Listar Produto pela ID
        public Produto ObterProdutoPelaId(int id)
        {
            try
            {
                _cmd.CommandText = $"SELECT * FROM produtos WHERE id ={id}";

                SqlDataReader reader = _cmd.ExecuteReader();
                reader.Read();

                Produto produto = new Produto();
                produto.id = (int)reader[0];
                produto.Descricao = (string)reader[1];
                produto.Valor = (decimal)reader[2];

                reader.Close();
                return produto;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Erro carregar produto");
                return null;
            }
            finally
            {
                //Desconetar();
            }
        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------//
        //Cadastrar Venda
        public bool CadastrarVenda(int IdVendedor)
        {
            try
            {
                _cmd.CommandText = "INSERT INTO vendas (Vendedor, mStatus) VALUES (@idVendedor, 'Aguardando pagamento')";

                _cmd.Parameters.AddWithValue("@idVendedor", IdVendedor);

                _cmd.ExecuteNonQuery();

                Console.WriteLine("Cadastrar venda - OK");
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Erro ao cadastrar venda");

                return false;
            }
            finally
            {
                Desconetar();
            }
        }

        //Listar todos Vendedores
        public List<Venda> ObterVendas()
        {
            try
            {
                _cmd.CommandText = "SELECT id FROM vendas";

                SqlDataReader reader = _cmd.ExecuteReader();

                List<Venda> vendas = new List<Venda>();
                List<int> ids = new List<int>();

                while (reader.Read())
                {
                    ids.Add((int)reader[0]);
                }

                reader.Close();

                for (int i = 0; i < ids.Count; i++)
                {
                    Venda venda = new Venda();
                    venda = ObterVendaPelaId(ids[i]);
                    vendas.Add(venda);
                }

                return vendas;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Erro carregar produtos");
                return null;
            }
            finally
            {
                Desconetar();
            }
        }

        //Listar Vendedor pela ID
        public Venda ObterVendaPelaId(int id)
        {
            try
            {
                _cmd.CommandText = $"SELECT * FROM vendas WHERE id ={id}";

                SqlDataReader reader = _cmd.ExecuteReader();
                reader.Read();

                int idVendedor = (int)reader[1];
                string produtos = (string)reader[2];
                string[] produtosArr = produtos.Split(";");

                Venda venda = new Venda();
                venda.id = (int)reader[0];
                venda.Status = (string)reader[3];

                reader.Close();

                venda.Vendedor = ObterVendedoresPelaId(idVendedor);

                List<Produto> produtosL = new List<Produto>();
                for (int i = 1; i < produtosArr.Length; i++)
                {
                    Produto produto = new Produto();
                    produto = ObterProdutoPelaId(Convert.ToInt32(produtosArr[i]));
                    produtosL.Add(produto);
                }

                venda.Produtos = produtosL;


                return venda;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Erro carregar venda");
                return null;
            }
            finally
            {
                //Desconetar();
            }
        }

        public bool AdicionarProduto(int idVenda, int idProduto)
        {
            try
            {
                _cmd.CommandText = $"SELECT Produtos FROM vendas WHERE id ={idVenda}";

                SqlDataReader reader = _cmd.ExecuteReader();
                reader.Read();

                string produtos;

                try
                {
                    produtos = (string)reader[0];
                }
                catch
                {
                    produtos = "";
                }

                reader.Close();

                produtos = produtos + ";" + idProduto;

                _cmd.CommandText = $"UPDATE vendas SET Produtos = '{produtos}' WHERE id ={idVenda}";
                _cmd.ExecuteNonQuery();

                Console.WriteLine("Produto adicionado");
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return false;
        }

        public bool AlterarStatus(int idVenda, string status)
        {
            try
            {
                _cmd.CommandText = $"UPDATE vendas SET mStatus = '{status}' WHERE id={idVenda}";
                _cmd.ExecuteNonQuery();
                Console.WriteLine("Aqui");

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
    }
}