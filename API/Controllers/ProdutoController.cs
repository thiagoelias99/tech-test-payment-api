using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using tech_test_payment_api.Models;
using tech_test_payment_api.Connections;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProdutoController : ControllerBase
    {
        //Conectar no DB
        private readonly SqlServerConnection _connection = new SqlServerConnection();

        public ProdutoController()
        {
            _connection.Conectar();
        }

        [HttpPost]
        public IActionResult Adicionar(Produto produto)
        {
            return _connection.CadastrarProduto(produto) ?
            CreatedAtAction(nameof(ObterPorId), new { id = produto.id }, produto) :
            StatusCode(500);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var produto = _connection.ObterProdutoPelaId(id);

            if (produto == null)
            {
                return NotFound();
            }
            return Ok(produto);
        }

        [HttpGet("ObterTodos")]
        public IActionResult ObterTodos()
        {
            var produtos = _connection.ObterProdutos();
            return Ok(produtos);
        }
    }
}