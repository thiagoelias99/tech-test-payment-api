using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using tech_test_payment_api.Models;
using tech_test_payment_api.Connections;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        //Conectar no DB
        private readonly SqlServerConnection _connection = new SqlServerConnection();
        
        public VendaController()
        {
            _connection.Conectar();
            Console.WriteLine("Construtor");
        }

        [HttpPost("Nova Venda/{idVendedor}")]
        public IActionResult CriarVenda(int idVendedor)
        {
            return _connection.CadastrarVenda(idVendedor) ?
            Ok() :
            StatusCode(500);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _connection.ObterVendaPelaId(id);

            if (venda == null)
            {
                return NotFound();
            }
            return Ok(venda);
        }

        [HttpPatch("Adicionar Produto/{idVenda}/{idProduto}")]
        public IActionResult AdicionarProduto(int idVenda, int idProduto)
        {

            if (_connection.AdicionarProduto(idVenda, idProduto))
            {
                return Ok();
            }
            return NotFound();
        }

        [HttpGet("ObterTodos")]
        public IActionResult ObterVendas()
        {
            var produtos = _connection.ObterVendas();
            return Ok(produtos);
        }

        [HttpPatch("Alterar Status/{idVenda}/{status}")]
        public IActionResult AdicionarProduto(int idVenda, string status)
        {
            var venda = _connection.ObterVendaPelaId(idVenda);

            switch (status)
            {
                case "Pagamento Aprovado":
                    switch (venda.Status)
                    {
                        case "Aguardando pagamento":
                            break;
                        default:
                            return StatusCode(500);
                    }
                    break;
                case "Enviado para Transportador":
                    switch (venda.Status)
                    {
                        case "Pagamento Aprovado":
                            break;
                        default:
                            return StatusCode(500);
                    }
                    break;
                case "Entregue":
                    switch (venda.Status)
                    {
                        case "Enviado para Transportador":
                            break;
                        default:
                            return StatusCode(500);
                    }
                    break;
                case "Cancelada":
                    switch (venda.Status)
                    {
                        case "Aguardando pagamento":
                        case "Pagamento Aprovado":
                            break;
                        default:
                            return StatusCode(500);
                    }
                    break;
                default:
                    return StatusCode(500);
            }

            _connection.AlterarStatus(idVenda, status);
            return Ok();
        }
    }
}