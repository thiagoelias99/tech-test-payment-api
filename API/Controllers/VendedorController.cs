using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using tech_test_payment_api.Models;
using tech_test_payment_api.Connections;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        //Conectar no DB
        private readonly SqlServerConnection _connection = new SqlServerConnection();

        public VendedorController()
        {
            _connection.Conectar();
        }

        [HttpPost]
        public IActionResult Adicionar(Vendedor vendedor)
        {
            return _connection.CadastrarVendedor(vendedor) ?
            CreatedAtAction(nameof(ObterPorId), new { id = vendedor.id }, vendedor) :
            StatusCode(500);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var vendedor = _connection.ObterVendedoresPelaId(id);

            if (vendedor == null)
            {
                return NotFound();
            }
            return Ok(vendedor);
        }

        [HttpGet("ObterTodos")]
        public IActionResult ObterTodos()
        {
            var vendedores = _connection.ObterVendedores();
            return Ok(vendedores);
        }
    }
}