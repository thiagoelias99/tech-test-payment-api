namespace tech_test_payment_api.Models
{
    public enum StatusVendaEnum
    {
        Pendente,
        Pagamento_aprovado,
        Enviado_para_transportadora,
        Entregue,
        Cancelada
    }
}