using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Tabelas
    {
        public Dictionary<string, string> tabelas = new Dictionary<string, string>();

        public Tabelas()
        {
            tabelas.Add("vendedores",
            "CREATE TABLE vendedores("
                + "id INT IDENTITY(1, 1) PRIMARY KEY,"
                + "Nome VARCHAR(40),"
                + "SobreNome VARCHAR(40));");

            tabelas.Add("produtos",
        "CREATE TABLE produtos("
            + "id INT IDENTITY(1, 1) PRIMARY KEY,"
            + "Descricao VARCHAR(100),"
            + "Valor DECIMAL(8,2));");

            tabelas.Add("vendas",
        "CREATE TABLE vendas("
            + "id INT IDENTITY(1, 1) PRIMARY KEY,"
            + "Vendedor INT,"
            + "Produtos VARCHAR(MAX),"
            + "mStatus VARCHAR (25));");
        }
    }
}